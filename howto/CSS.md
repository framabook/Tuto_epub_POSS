# Les fichiers CSS 

![fichiers CSS](https://framagit.org/framabook/POSS_2106/raw/55df5250fa2c922433ba56f5da924a6c0a4d1b20/howto/images/cssFiles.png)

## 2 fichiers communs aux framabooks : 

* `framabook.css`
  * il sert de maître aux deux autres fichiers .css 
  * il est appelé par les fichiers .xhtml qui contiendront le corps des chapitres

* `relecture.css` : il attribue un code couleur aux classes sémantiques, ce qui me permet une relecture de vérification visuelle plus facile. Par exemple, cela pemet de vérifier que les mots en langue étrangère ont bien la balise `<i>`, que les abréviations sont bien identifiées etc. 

## 1 fichier spécifique pour cet ouvrage, que je vais créer : 

* `poss.css` qui va contenir :    
  * une classe spéciale pour les questions-réponses, assez nombreuses dans cet ouvrage qui comporte beaucoup d’interviews.  
  * les classes nécessaires auteur-intro et auteur, question



