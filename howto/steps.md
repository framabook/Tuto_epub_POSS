# Les étapes d’un Framabook : du fichier .md au .epub accessible.
## L'exemple de l'epub POSS 2016


### 1. La structure du livre 
  
##### 1.1 Parcourir le fichier .md ou le .pdf (suivant ce dont on dispose).

* Observer particulièrement la **table des matières**, qui permet de comprendre la structure du livre et de savoir comment on va organiser son travail.  
* Repérer quelles sont les particularités du texte pour savoir quelles *classes* de .css spécifiques seront nécessaires. Ici par exemple, on trouve de nombreuses interviews.

 ![structureLivre](/howto/images/structureLivre.png)  
   
   
##### 1.2 Déterminer en **combien de fichiers** on va découper le contenu du livre.  
* Il n’existe pas de règles pour le découpage, il est recommandé de veiller à ce que les fichiers .xhtml ne soient pas trop lourds. J’ai eu un roman, tout simple sans images, 
qui n’avait qu’un fichier pour l’ensemble du texte. le texte ne s'affichait donc que très lentement : on pouvait tourner la page et la lire avant que la liseuse n'affiche la page suivante.
* Chaque livre est différent. Il faut donc voir avec le responsable de l’édition papier pour vérifier qu'on a bien compris la structure du livre.  

* Dans le cas de notre exemple, comme le livre n’est pas très gros (environ 138 pages en PDF), on peut constituer 8 chapitres et des sous-chapitres, plus les autres fichiers. J’ai choisi de faire un fichier par partie avec dans « body epub:type="bodymatter part" »
 puis section epub:type="bodymatter chapter".
  

##### 1.3 Modifier les identifiants des chapitres pour qu’ils soient plus simples. 
Comme la table des matières sera faite à la main, il sera plus facile d’avoir des `id` du type :`ch1-1`  plutôt que :  `reprendre-le-contrôle-sur-ses-données-oui-mais-pour-quoi-faire`


### 2. Les fichiers du livre   

##### 2.1  Création du fichier `chapitre1.xhtml` qui servira de base pour tous les autres fichiers de chapitre. 

Ici je dispose d’[un fichier source unique](/ouvrageposs2016entier.md) pour l’ensemble du livre donc je copie-colle la partie du texte du fichier source dans le fichier chapitre1.xhtml

##### 2.2 Le fichier `container.xml`
* Il est situé dans le répertoire **META-INF** qu'il faut d'abord créer
![fichier container.xml](/howto/images/container.png)
* Il indique le nom dossier où seront rangés les fichiers de contenu. Ce sont des fichiers aux formats OPS soit OEBPS. En général j’utilise OPS.  

##### 2.3 Le fichier `mimetype`

* Il est situé à la racine de l'epub
![fichier mimetype](/howto/images/FichierMimetype.png)
* Il comprend une seule ligne :
`application/epub+zip`
* C'est toujours le même fichier quel que soit le livre


##### 2.4 Les fichiers CSS 

![fichiers CSS](/howto/images/cssFiles.png)

###### 2 fichiers communs aux framabooks : 

* `framabook.css`
  * il sert de maître aux deux autres fichiers .css 
  * il est appelé par les fichiers .xhtml qui contiendront le corps des chapitres

* `relecture.css` : il attribue un code couleur aux classes sémantiques, ce qui me permet une relecture de vérification visuelle plus facile. Par exemple, cela pemet de vérifier que les mots en langue étrangère ont bien la balise `<i>`, que les abréviations sont bien identifiées etc. 

##### 1 fichier spécifique pour cet ouvrage, que je vais créer : 

* `poss.css` qui va contenir :    
  * une classe spéciale pour les questions-réponses, assez nombreuses dans cet ouvrage qui comporte beaucoup d’interviews.  
  * les classes nécessaires auteur-intro et auteur, question
